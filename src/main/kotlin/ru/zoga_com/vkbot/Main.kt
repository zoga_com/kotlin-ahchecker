package ru.zoga_com.vkbot

import ru.zoga_com.vkbot.utils.YamlParser
import ru.zoga_com.vkbot.utils.Polling
import ru.zoga_com.vkbot.utils.Database
import ru.zoga_com.vkbot.commands.BazaarCommand

class Main {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            YamlParser().initConfig()
            println("Включение лонгполла")
            Polling().initCommands()
            Database.connect()
            BazaarCommand.fillProducts()
            Polling().startPolling()
        }
    }
}
