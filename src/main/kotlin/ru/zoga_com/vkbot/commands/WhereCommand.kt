package ru.zoga_com.vkbot.commands

import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.utils.Utils
import api.longpoll.bots.model.events.messages.MessageNew
import org.json.JSONObject

object WhereCommand: Command() {
    override fun onCommand(args: Array<String>, message: MessageNew) {
        try {
            if(args.size == 2) {
                val player: JSONObject = JSONObject(Utils().getJson("https://api.slothpixel.me/api/players/${args[1]}"))
                var text: String = ""
                when(player.getBoolean("online")) {
                    true -> {
                        text = "Игрок ${player.getString("username")} онлайн на сервере ${player.getString("last_game")} (Сессия длится уже ${Utils().formatTimeInHours(System.currentTimeMillis() - player.getLong("last_login") - 10800000)})"
                    }
                    false -> {
                        text = "Игрок был онлайн ${Utils().formatTime(player.getLong("last_logout"))} (Сессия длилась ${Utils().formatTimeInHours(player.getLong("last_logout") - player.getLong("last_login") - 10800000)})"
                    }
                }
                Utils().sendMessage(text, message.getMessage().getPeerId())
            }
        } catch(e: Exception) {
            Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName} (Игрок не существует?)", message.getMessage().getPeerId())
            e.printStackTrace()
        }
    }
}
