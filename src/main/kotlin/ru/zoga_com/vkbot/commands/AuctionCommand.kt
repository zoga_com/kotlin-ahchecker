package ru.zoga_com.vkbot.commands

import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.types.User
import ru.zoga_com.vkbot.utils.Utils
import ru.zoga_com.vkbot.utils.db.Users
import api.longpoll.bots.model.events.messages.MessageNew
import org.json.JSONObject
import org.json.JSONArray
import java.text.DecimalFormat 

object AuctionCommand: Command() {
    val format: DecimalFormat = DecimalFormat("#,###")

    override fun onCommand(args: Array<String>, message: MessageNew) {
        try {
            if(args.size == 2) {
                    Utils().sendDeletingMessage(builder(Utils().getPlayerAuctions(Utils().getUUID(args[1])), args[1]), message.getMessage().getPeerId())
            }
            if(args.size == 1) {
                if(Utils().createUserIfNotExists(Users.getUser(message.getMessage().getFromId(), "cock_length"), message)) {
                    if(User(message.getMessage().getFromId().toLong()).nick != "false") {
                        Utils().sendDeletingMessage(builder(Utils().getPlayerAuctions(Utils().getUUID(User(message.getMessage().getFromId().toLong()).nick)), User(message.getMessage().getFromId().toLong()).nick), message.getMessage().getPeerId())
                    } else { Utils().sendMessage("Вы ещё не установили ник.", message.getMessage().getPeerId()) }
                } else { Utils().sendMessage("Ваш профиль не найден, поэтому сейчас он будет создан.\n\nВыполните команду снова.", message.getMessage().getPeerId()) }
            }
        } catch(e: Exception) {
            Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName} (Игрок не существует?)", message.getMessage().getPeerId())
            e.printStackTrace()
        }
    }

    private fun builder(json: JSONObject, nick: String): String {
        val coinFormat: Array<String> = arrayOf("коин", "коина", "коинов")
        var earned: Long = 0
        var return_message: String = ""
        return_message = return_message + "📗 Аукционы игрока ${nick}:\n\n"
        val aucs: JSONArray = json.getJSONArray("auctions")
        var aucCount: Int = 0
        for (i in 0 until aucs.length()) {
            var icon: String
            var type: String
            var status: String = ""
            var bids: String
            val auc: JSONObject = aucs.getJSONObject(i)
            if (!auc.getBoolean("claimed")) {
                aucCount += 1
                if (auc.getLong("highest_bid_amount") == (0).toLong()) {
                    bids = "• 💸 Начальная ставка: ${format.format(auc.getLong("starting_bid"))} ${Utils().findDeclension(auc.getLong("starting_bid"), coinFormat)}"
                } else {
                    bids = "• 💸 Последняя ставка: ${format.format(auc.getLong("highest_bid_amount"))} ${Utils().findDeclension(auc.getLong("highest_bid_amount"), coinFormat)}\n• Ставка от игрока: ${Utils().getNick(auc.getJSONArray("bids").getJSONObject(auc.getJSONArray("bids").length() - 1).getString("bidder"))}"
                }
                if (System.currentTimeMillis() > auc.getLong("end")) {
                    if (auc.getJSONArray("bids").length() >= 1) {
                        icon = "✔ "
                        earned += auc.getLong("highest_bid_amount")
                    } else { 
                        icon = "🚫 " 
                    }
                } else {
                    icon = "⏳ "
                    status = "• Истекает ${Utils().formatTime(auc.getLong("end")).lowercase()}\n"
                }
                if (auc.equals("bin")) {
                    type = "BIN"
                } else {
                    type = "Аукцион"
                }
                return_message = return_message + "${icon}[${auc.getString("tier")}] ${auc.getString("item_name")}\n${status}${bids}\n• 💭 Тип: ${type}\n\n"
            }
        }
        if(aucCount == 0) { return_message = return_message + "У игрока нет аукционов." }
        return_message = return_message + "\n\n🌍 Всего можно забрать: ${format.format(earned)}"
        return return_message
    }
}
