package ru.zoga_com.vkbot.utils

import api.longpoll.bots.LongPollBot
import api.longpoll.bots.model.events.messages.MessageNew
import api.longpoll.bots.model.events.messages.MessageEvent
import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.commands.TestCommand
import ru.zoga_com.vkbot.commands.BazaarCommand
import ru.zoga_com.vkbot.commands.ArmorCommand
import ru.zoga_com.vkbot.commands.CockCommand
import ru.zoga_com.vkbot.commands.SetNameCommand
import ru.zoga_com.vkbot.commands.ProfileCommand
import ru.zoga_com.vkbot.commands.StatsCommand
import ru.zoga_com.vkbot.commands.WhereCommand
import ru.zoga_com.vkbot.commands.CatsCommand
import ru.zoga_com.vkbot.commands.HelpCommand
import ru.zoga_com.vkbot.commands.AuctionCommand
import org.json.JSONObject

class Polling: LongPollBot() {
    companion object {
        @JvmStatic
        var commands: HashMap<String, Command> = hashMapOf()
    }

    override fun onMessageNew(message: MessageNew) {
        var args: Array<String> = message.getMessage().getText().split(" ").toTypedArray()
        Utils().triggerKillCat(message)
        if(message.getMessage().getText().startsWith("/") && commands.get(args[0].lowercase()) != null) {
            val commandClass: Command? = commands.get(args[0].lowercase())
            commandClass?.onCommand(args, message)
            Runtime.getRuntime().gc()
        }
    }

    override fun onMessageEvent(message: MessageEvent) {
        if(JSONObject(message.payload.toString()).getBoolean("deleteMessage")) {
            vk.messages.delete().setDeleteForAll(true).setConversationMessageIds(message.conversationMessageId.toInt()).setPeerId(message.peerId).executeAsync()
        }
    }

    override fun getAccessToken(): String? {
        return YamlParser().getConfigValue("token")
    }

    fun initCommands() {
        commands.put("/test", TestCommand)
        commands.put("/bz", BazaarCommand)
        commands.put("/бз", BazaarCommand)
        commands.put("/bazaar", BazaarCommand)
        commands.put("/базар", BazaarCommand)
        commands.put("/armor", ArmorCommand)
        commands.put("/броня", ArmorCommand)
        commands.put("/cock", CockCommand)
        commands.put("/кок", CockCommand)
        commands.put("/setname", SetNameCommand)
        commands.put("/ник", SetNameCommand)
        commands.put("/name", SetNameCommand)
        commands.put("/setnick", SetNameCommand)
        commands.put("/profile", ProfileCommand)
        commands.put("/п", ProfileCommand)
        commands.put("/p", ProfileCommand)
        commands.put("/проф", ProfileCommand)
        commands.put("/профиль", ProfileCommand)
        commands.put("/prof", ProfileCommand)
        commands.put("/stats", StatsCommand)
        commands.put("/s", StatsCommand)
        commands.put("/st", StatsCommand)
        commands.put("/с", StatsCommand)
        commands.put("/ст", StatsCommand)
        commands.put("/стата", StatsCommand)
        commands.put("/where", WhereCommand)
        commands.put("/где", WhereCommand)
        commands.put("/котята", CatsCommand)
        commands.put("/cats", CatsCommand)
        commands.put("/help", HelpCommand)
        commands.put("/помощь", HelpCommand)
        commands.put("/хелп", HelpCommand)
        commands.put("/ah", AuctionCommand)
        commands.put("/ах", AuctionCommand)
        commands.put("/аук", AuctionCommand)
        println("Команды загружены.")
    }
}
