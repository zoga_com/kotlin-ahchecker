package ru.zoga_com.vkbot.utils

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException

object Database {
    var connection: Connection = DriverManager.getConnection("jdbc:mysql://${YamlParser().getConfigValue("mysql-address")}/${YamlParser().getConfigValue("mysql-database")}?user=${YamlParser().getConfigValue("mysql-user")}&password=${YamlParser().getConfigValue("mysql-password")}")

    fun connect() {
        connection.createStatement().execute("CREATE TABLE IF NOT EXISTS users(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,vk_id INT,last_cock LONG,cock_length INT,nick VARCHAR(128),longest_cock INT,register_date LONG);")
        connection.createStatement().execute("CREATE TABLE IF NOT EXISTS cats(killed INT NOT NULL PRIMARY KEY, scope VARCHAR(128));")
        println("База данных инициализирована.")
        this.createCatsGlobal()
    }

    fun get(query: String): ResultSet {
        checkConnection()
        return connection.prepareStatement(query).executeQuery()
    }

    fun insert(query: String) {
        checkConnection()
        connection.prepareStatement(query).executeUpdate()
    }

    private fun createCatsGlobal() {
        val result: ResultSet = get("SELECT killed FROM cats WHERE scope = \"ALL\"")
        if(!result.next()) { this.insert("INSERT INTO cats(killed,scope) VALUES(0,\"ALL\");") }
    }

    fun createPeerCatsCounterIfNotExists(peer: Long) {
        val result: ResultSet = get("SELECT killed FROM cats WHERE scope = \"${peer}\"")
        if(!result.next()) { this.insert("INSERT INTO cats(killed,scope) VALUES(0,\"${peer}\");") }
    }

    private fun checkConnection() {
        try {
            connection.prepareStatement("/* ping */ SELECT 1").executeQuery()
            return
        } catch(e: SQLException) {
            connect()
            println("База данных переподключена.")
        }
    }
}